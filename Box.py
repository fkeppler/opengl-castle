import os
import numpy as N
from OpenGL.GL import *
from OpenGL.GL.shaders import compileShader, compileProgram
from databuffer import DataBuffer
from texture import loadTexture
from transforms import *
class Box():
    def __init__(self,
                 height = 10.0,
                 width = 1.0,
                 length = 1.0,
                 origin = translationMatrix(0,0,0),
                 texDensity = 0,
                 vertShader = 'vertex.vs',
                 fragShader = 'TextureShader.fs',
                 textureName = 'grassfloor.png',
                 shaderPositionName = 'position',
                 shaderNormalName = 'normal',
                 shaderTexcoordName = 'texcoord'):
        self.height = height
        self.width = width
        self.length = length
        self.origin = origin
        self.texDensity = texDensity
        if texDensity == 0:
            self.texDensity = height/4
        with open(os.path.join(os.getcwd(), vertShader)) as fp:
            vert = fp.read()
        with open(os.path.join(os.getcwd(), fragShader)) as fp:
            frag = fp.read()
        try:
            self.program = compileProgram(
                compileShader(vert, GL_VERTEX_SHADER),
                compileShader(frag, GL_FRAGMENT_SHADER))
        except RuntimeError as rte:
            print rte[0]
            raise
        self.Texture = loadTexture(os.path.join("images",textureName))
        # get a handle
        self.TextureID = glGetUniformLocation(self.program, "myTextureSampler")
        
        self.positionLocation = glGetAttribLocation(self.program,
                                                    shaderPositionName)
        self.normalLocation = glGetAttribLocation(self.program,
                                                  shaderNormalName)
        self.texcoordLocation = glGetAttribLocation(self.program,
                                                    shaderTexcoordName)
        self.makeDataBuffers()
    
    def quad(self,x,z,step):
        x1 = x+step
        z1 = z+step
        a = N.array((x,self.domeHeight(x,z),z),dtype=N.float32)
        b = N.array((x,self.domeHeight(x,z1),z1),dtype=N.float32)
        c = N.array((x1,self.domeHeight(x1,z1),z1),dtype=N.float32)
        d = N.array((x1,self.domeHeight(x1,z),z),dtype=N.float32)
        norm = N.cross(b-a,d-a)
        norm = norm/N.sqrt(N.dot(norm,norm))
        self.texcoords.extend((x,z,x1,z1,x1,z,x,z,x,z1,x1,z1))
        for pt in (a,c,d,a,b,c):
            self.positions.extend(pt)
            self.positions.append(1.0)
            self.normals.extend(norm)
            self.normals.append(0.0)

    def makeDataBuffers(self):
        #place where buffers for creating vertices are formed
        length = self.length
        height = self.height
        width = self.width
        #front of cube:
        a = [0,0, length, 1]
        b = [width,0, length, 1]
        c = [width, height, length, 1]
        d = [0, height, length, 1]
        #back of cube:
        e = [0,0,0, 1]
        f = [width,0,0, 1]
        g = [width, height,0, 1]
        h = [0, height,0, 1]
        positions =  a+b+c + c+d+a # front
        positions += d+c+g + g+h+d # top
        positions += h+g+f + f+e+h # back
        positions += e+f+b + b+a+e # bottom
        positions += e+a+d + d+h+e # left
        positions += b+f+g + g+c+b # right
        
        top = [0,1,0,0]
        right = [1,0,0,0]
        back = [0,0,-1,0]
        left = [-1,0,0,0]
        front = [0,0,1,0]
        bottom = [0,-1,0,0]
        normals  = 6*front + 6*top + 6*back + 6*bottom + 6*left + 6*right
        hlratio = self.height/self.length
        hwratio = self.height/self.width
        #hratio = self.length/self.height
        hratio = 1
        #texs = 2*[0,0,  hlratio,0,  hlratio,hratio,     hlratio,hratio,  0,hratio,  0,0] + 4*[0,0,  hlratio,0,  hlratio,hratio,     hlratio,hratio,  0,hratio,  0,0]
        texs = 3*[0,0,  hlratio,0,  hlratio,hratio,     hlratio,hratio,  0,hratio,  0,0] + 3*[0,0,  hwratio,0,  hwratio,hratio,     hwratio,hratio,  0,hratio,  0,0]
        texcoords = [self.texDensity*i for i in texs]
        
        self.positions = DataBuffer(positions, 4, self.positionLocation, 0, 4)
        self.normals = DataBuffer(normals, 4, self.normalLocation, 0, 4)
        self.texcoords = DataBuffer(texcoords, 2, self.texcoordLocation, 0, 2)
        self.n = len(positions)/4


    def draw(self, uniforms1f, uniforms4fv, uniformMatrices):
        glUseProgram(self.program)
        #bind the texture
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, self.Texture)
        # set our sampler
        glUniform1i(self.TextureID, 0)
        
        uniformMatrices['mMatrix'] = N.dot(uniformMatrices['mMatrix'], self.origin)
        for a in uniforms1f:
            loc = glGetUniformLocation(self.program, a)
            glUniform1f(loc, uniforms1f[a])
        for a in uniforms4fv:
            loc = glGetUniformLocation(self.program, a)
            glUniform4fv(loc, 1, uniforms4fv[a])
        for a in uniformMatrices:
            loc = glGetUniformLocation(self.program, a)
            glUniformMatrix4fv(loc, 1, True, uniformMatrices[a])
        bufs = (self.positions, self.texcoords)
        for buf in bufs: buf.Start()
        glDrawArrays(GL_TRIANGLES, 0, self.n)
        for buf in bufs: buf.Stop()
        glUseProgram(0)
                


        
                
        
        
