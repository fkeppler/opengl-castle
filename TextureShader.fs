#version 330 core

// Values that stay constant for the whole mesh.
uniform sampler2D myTextureSampler;

// Interpolated values from the vertex shaders
in vec4 fragmentPosition;
in vec4 fragmentTexposition;
in vec4 fragmentNormal;
in vec4 fragmentUpVector;
in vec2 fragmentTexcoord;
in vec4 eyeVector;

// Ouput data
out vec4 color;



void main(){

	// Output color = color of the texture at the specified Texcoord
	color = texture( myTextureSampler, fragmentTexcoord );
}
