from transforms import *

class World():
    """stores all the objects in the world and provides methods to display them"""    
    def __init__(self, *objs):
        self.objects = []
        for i in objs:
            self.objects.append(i)

    def draw(self, uniforms1f, uniforms4fv, uniformMatrices):
        for obj in self.objects:
            obj.draw(uniforms1f, uniforms4fv, uniformMatrices)
            uniformMatrices['mMatrix'] = translationMatrix(0,0,0)
