import os
import numpy as N
from OpenGL.GL import *
from OpenGL.GL.shaders import compileShader, compileProgram
from databuffer import DataBuffer
import pygame
from pygame.locals import *
from texture import loadTexture
from transforms import *
from texturedTower import texTower
import copy
class castleTower():
    """puts pieces of the castle towers together. FAILED EXPERIMENT"""
    def __init__(self,
                 height = 10.0,
                 radius = 5,
                 origin = translationMatrix(0,0,0),
                 bottomTexture = 'stonefloor.png',
                 topTexture = 'tutor.png',
                 roofTexture = 'straw.png'):
        self.origin = origin
        self.bottom = texTower(height=height,
                                    radius=radius,
                                    origin=N.dot(self.origin, translationMatrix(0,0,0)),
                                    textureName=bottomTexture)
        self.top = texTower(height=height/3,
                            radius=radius*.8,
                            squat=1.0,
                            origin=N.dot(self.origin, translationMatrix(0,height,0)),
                            textureName=topTexture)
        self.roof = texTower(height = 1,
                             radius=radius*1,
                             squat=.01,
                             textureName=roofTexture,
                             origin=N.dot(self.top.origin, translationMatrix(0,height/2,0)))
    def draw(self, uniforms1f, uniforms4fv, uniformMatrices):
        bottomMatrices = copy.deepcopy(uniformMatrices)
        roofMatrices = copy.deepcopy(uniformMatrices)
        self.top.draw(uniforms1f, uniforms4fv, uniformMatrices)
        self.bottom.draw(uniforms1f, uniforms4fv, bottomMatrices)
        self.roof.draw(uniforms1f, uniforms4fv, roofMatrices)
