#Note:  since normals are flat across the cube, we don't interpolate
# surface normals (Phong-style) over the primitives.
# We DO interpolate the eye and light vectors for the frag shader

import os

from OpenGL.GL import *
from OpenGL.GL.shaders import compileShader, compileProgram

import pygame
from pygame.locals import *

import numpy as N
from ctypes import c_void_p

from transforms import *
from databuffer import DataBuffer
from floor import Floor
from frame import Frame
from texturedTower import texTower
from texturedFloor import texFloor
from Box import Box
from World import World
from castleTower import castleTower
def loadFile(filename):
    with open(os.path.join(os.getcwd(), filename)) as fp:
        return fp.read()

def display():
    global time, light, frame, world, Door1, pMatrix
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    vMatrix =  frame.matrix

    xoffset = 0
    zoffset = 0

    trans = translationMatrix(-xoffset,0,-zoffset)
    Door1.draw({},
               {},
               {'vMatrix':vMatrix,
                'pMatrix':pMatrix,
                'mMatrix':translationMatrix(0,5*N.sin(time/16)-10,0)})
    world.draw({},
               {},
               {'vMatrix':vMatrix,
                'pMatrix':pMatrix,
                'mMatrix':trans})
                      
def initializeVertexArray():
    # Must have a vertex array object to use vertex buffer objects.
    # Just one will do:
    global vao_array
    vao_array = N.zeros(1, dtype=N.uint)
    #glGenVertexArrays(1, vao_array)
    vao_array = glGenVertexArrays(1)
    glBindVertexArray(vao_array)


# Must be called after we have an OpenGL context, i.e. after the pygame
# window is created
def init():
    global  cubeBuffers,textureBuffers, world, Door1, pMatrix
    wallTexture = 'wall2.png'
    towerTexture = "wall2.png"
    houseTexture= 'tutor.png'
    floorheight = -.5
    castleLength = 50
    castleWidth = 50
    castleWallHeight = 10
    pMatrix = projectionMatrix(0.1, 1000.0, .10, 0.075)
    towerRadius = 5.0
    towerHeight = 13
    Door1 = Box(origin=translationMatrix(castleWidth*1/3,floorheight+castleWallHeight/2,castleLength),
                width=castleWidth/3+1,
                height=castleWallHeight/2,
                textureName=wallTexture)
                
    world = World(texFloor(textureName='grassfloor.png'),
                    Box(origin=translationMatrix(0,floorheight,0),
                        width=castleWidth,
                        height=castleWallHeight,
                        textureName=wallTexture),
                    Box(origin=translationMatrix(0,floorheight,0),
                        length=castleLength,
                        height=castleWallHeight,
                        textureName=wallTexture),
                    Box(origin=translationMatrix(castleWidth,floorheight,0),
                        length=castleLength,
                        height=castleWallHeight,
                        textureName=wallTexture),
                    Box(origin=translationMatrix(0,floorheight,castleLength),
                        width=castleWidth/3,
                        height=castleWallHeight,
                        textureName=wallTexture),
                    Box(origin=translationMatrix(castleWidth*2/3,floorheight,castleLength),
                        width=castleWidth/3,
                        height=castleWallHeight,
                        textureName=wallTexture),
                    Box(origin=translationMatrix(castleWidth*1/3,floorheight+castleWallHeight/2,castleLength),
                        width=castleWidth/3+1,
                        height=castleWallHeight/2,
                        textureName=wallTexture),
                    castleTower(origin=translationMatrix(castleWidth,0,castleLength),
                                bottomTexture=towerTexture,
                                topTexture=houseTexture,
                                height=towerHeight,
                                radius=towerRadius),
                    castleTower(origin=translationMatrix(0,0,castleLength),
                                bottomTexture=towerTexture,
                                topTexture=houseTexture,
                                height=towerHeight,
                                radius=towerRadius),
                    castleTower(origin=translationMatrix(castleWidth,0,0),
                                bottomTexture=towerTexture,
                                topTexture=houseTexture,
                                height=towerHeight,
                                radius=towerRadius),
                    castleTower(origin=translationMatrix(0,0,0),
                                bottomTexture=towerTexture,
                                topTexture=houseTexture,
                                height=towerHeight,
                                radius=towerRadius),
                    Box(width=400,
                        height=400,
                        length=400,
                        texDensity=.5,
                        textureName='space.jpg',
                        origin=translationMatrix(-200,-200,-200)))
                                
                    
                        
    
    initializeVertexArray()    
    glClearColor(0.0, 0.0, 0.0, 0.0)
    glEnable(GL_DEPTH_TEST)
    #glEnable(GL_CULL_FACE)
       
def main():
    global window, time, light, inc, whichTex, frame
    pygame.init()
    screen = pygame.display.set_mode((1280,960), OPENGL|DOUBLEBUF)#|FULLSCREEN)
    pygame.display.set_caption('a Developing Castle')

    screentoggle = False
    clock = pygame.time.Clock()
    time = 0.0
    light = N.array((10,10,10,0), dtype = N.float32)
    inc = 0.05
    whichTex = 0
    init()
    frame = Frame()
    while True:     
        for event in pygame.event.get():
            if event.type == QUIT:
                return
            if event.type == KEYUP and event.key == K_ESCAPE:
                return
            if event.type == KEYDOWN:
                if event.key == K_s:
                    if inc == 0.0:
                        inc = 0.2
                    else:
                        inc = 0.0

        pressed = pygame.key.get_pressed()
        moveSpeed = .25
        if pressed[K_LCTRL]:
            moveSpeed = 1
        if pressed[K_w]:
            frame.move(moveSpeed)
        if pressed[K_s]:
            frame.move(-moveSpeed)
        if pressed[K_UP]:
            frame.tilt(1.0)
        if pressed[K_DOWN]:
            if pressed[K_LSHIFT]:
                frame.pan(moveSpeed)
            frame.tilt(-1.0)
        if pressed[K_a]:
            frame.strafe(moveSpeed)
        if pressed[K_d]:
            frame.strafe(-moveSpeed)
        if pressed[K_LEFT]:
            if pressed[K_LSHIFT]:
                frame.strafe(moveSpeed)
            else:
                frame.rotate(1.0)
        if pressed[K_RIGHT]:
            if pressed[K_LSHIFT]:
                frame.strafe(-moveSpeed)
            else:
                frame.rotate(-1.0)
            
        clock.tick(30)
        time += inc
        display()
        pygame.display.flip()

if __name__ == '__main__':
    try:
        main()
    finally:
        pygame.quit()
