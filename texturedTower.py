import os
import numpy as N
from OpenGL.GL import *
from OpenGL.GL.shaders import compileShader, compileProgram
from databuffer import DataBuffer
import pygame
from pygame.locals import *
from texture import loadTexture
from transforms import *

class texTower():
    def __init__(self,
                 height = 20.0,
                 radius = 30,
                 squat = 0.5,
                 origin = translationMatrix(0,0,0),
                 textureName = 'stonefloor.png',
                 vertShader = 'vertex.vs',
                 fragShader = 'TextureShader.fs',
                 shaderPositionName = 'position',
                 shaderNormalName = 'normal',
                 shaderTexcoordName = 'texcoord'):
        self.height = height
        self.radius = radius
        self.origin = origin
        self.squat = squat
        with open(os.path.join(os.getcwd(), vertShader)) as fp:
            vert = fp.read()
        with open(os.path.join(os.getcwd(), fragShader)) as fp:
            frag = fp.read()

        try:
            self.program = compileProgram(
                compileShader(vert, GL_VERTEX_SHADER),
                compileShader(frag, GL_FRAGMENT_SHADER))
        except RuntimeError as rte:
            print rte[0]
            raise
        self.Texture = loadTexture(os.path.join("images",textureName))
        # get a handle
        self.TextureID = glGetUniformLocation(self.program, "myTextureSampler")
 
        self.positionLocation = glGetAttribLocation(self.program,
                                                    shaderPositionName)
        self.normalLocation = glGetAttribLocation(self.program,
                                                  shaderNormalName)
        self.texcoordLocation = glGetAttribLocation(self.program,
                                                    shaderTexcoordName)
        print(self.normalLocation)
        self.makeDataBuffers()
        #glUseProgram(0)

    def makeDataBuffers(self):
        positions = []
        normals = []
        texcoords = []
        squat = self.squat
        for angle in N.arange(0.0, 2.01*N.pi, 0.05*N.pi):
            s = N.sin(angle)
            c = N.cos(angle)
            r = self.radius
            h = self.height
            positions.extend((squat*r*s,   h+1, squat*r*c, 1.0))
            positions.extend((r*s, -1.0, r*c, 1.0))
            normals.extend((s, 0.0, c, 0.0))
            normals.extend((s, 0.0, c, 0.0))
            texcoords.extend((self.radius*1*angle/N.pi,   h/2))
            texcoords.extend((self.radius*1*angle/N.pi, 0.0))
        self.n = len(positions)/4
        self.positions = DataBuffer(positions, 4, self.positionLocation, 0, 4)
        self.normals = DataBuffer(normals, 4, self.normalLocation, 0, 4)
        self.texcoords = DataBuffer(texcoords, 2, self.texcoordLocation, 0, 2)

    def draw(self, uniforms1f, uniforms4fv, uniformMatrices):
        glUseProgram(self.program)
        #bind the texture
        glActiveTexture(GL_TEXTURE0)
        glBindTexture(GL_TEXTURE_2D, self.Texture)
        # set our sampler
        glUniform1i(self.TextureID, 0)
        uniformMatrices['mMatrix'] = N.dot(uniformMatrices['mMatrix'], self.origin)
        for a in uniforms1f:
            loc = glGetUniformLocation(self.program, a)
            glUniform1f(loc, uniforms1f[a])
        for a in uniforms4fv:
            loc = glGetUniformLocation(self.program, a)
            glUniform4fv(loc, 1, uniforms4fv[a])
        for a in uniformMatrices:
            loc = glGetUniformLocation(self.program, a)
            glUniformMatrix4fv(loc, 1, True, uniformMatrices[a])
        bufs = (self.positions, self.texcoords)
        for buf in bufs: buf.Start()
        glDrawArrays(GL_TRIANGLE_STRIP, 0, self.n)
        for buf in bufs: buf.Stop()
        glUseProgram(0)
