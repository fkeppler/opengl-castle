
#version 330
// Uniforms:
uniform mat4 mMatrix;
uniform mat4 vMatrix;
uniform mat4 pMatrix;
// Ins:
in vec4 position;
in vec4 normal;
in vec2 texcoord;
// Outs:
out vec2 fragmentTexcoord;

void main()
{
    fragmentTexcoord = texcoord;
    gl_Position = position;  
}
